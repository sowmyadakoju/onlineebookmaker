package com.book.store.services;

import com.book.store.models.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    public User getAuthorDetails(Long userId);
}
