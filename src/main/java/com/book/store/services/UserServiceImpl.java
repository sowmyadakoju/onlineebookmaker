package com.book.store.services;

import com.book.store.dao.UserDAO;
import com.book.store.exceptions.ResourceNotFoundException;
import com.book.store.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;



    @Override
    public User getAuthorDetails(Long userId) {
        return userDAO.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }
}
